//
//  SignInViewController.swift
//  TestFinalProject
//
//  Created by Chandrakanth on 4/22/20.
//  Copyright © 2020 Pavan Rao. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class SignInViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
    }
    
    func setUpElements(){
         
         errorLabel.alpha = 0
         
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(loginButton)
         
    }
    
    func validateFields() -> String? {
           
           //Check that all fields are filled in
           if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
               passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
               
               return "Please fill in all the fields"
           }
           
           //Check if password is secure
           let cleanedPasword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
           if Utilities.isPasswordValid(cleanedPasword) == false {
               return "Please make sure your password is at least 8 characters, contains a special character and a number"
           }
           return nil
    }
    
    @IBAction func login(_ sender: Any) {
        
        //Validate text fields
        let error = validateFields()
        
        if error != nil {
            
            showError(error!)
        }else{
            
            //Creating clean versions of email and password
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            //Signing in user
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                
                if error != nil {
                    
                    self.errorLabel.text = error?.localizedDescription
                    self.errorLabel.alpha = 1
                }else{
                    let role: String

                    role = "Admin"
                    if role == "Student" {
                        let studentHomeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.studentHomeViewController) as? StudentHomeController
                        self.view.window?.rootViewController = studentHomeViewController
                        self.view.window?.makeKeyAndVisible()
                    }
                    else {
                        let adminHomeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.adminHomeViewController) as? AdminHomeController
                        self.view.window?.rootViewController = adminHomeViewController
                        self.view.window?.makeKeyAndVisible()
                    }
                    
                }
            }
            
        }
    }
    
    func showError(_ message:String){
           
           errorLabel.text = message
           errorLabel.alpha = 1
    }
}

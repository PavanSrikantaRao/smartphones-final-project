//
//  JobDetailViewController.swift
//  TestFinalProject
//
//  Created by Pavan Rao on 4/23/20.
//  Copyright © 2020 Pavan Rao. All rights reserved.
//

import UIKit
import FirebaseFirestore

class JobAddViewController: UIViewController {

    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var deadlineTextField: UITextField!
    @IBOutlet weak var linkTextField: UITextField!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "SaveJob" {
            if (positionTextField.text == "" || companyTextField.text == "" || locationTextField.text == "" ||  deadlineTextField.text == "" || linkTextField.text == "") {
                let alert = UIAlertController(title: "Incorrect input", message: "All fields are mandatory!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {
        if segue.identifier == "SaveJob"{
            if let position = positionTextField.text,
            let company = companyTextField.text,
            let location = locationTextField.text,
            let deadline = deadlineTextField.text,
            let link = linkTextField.text{
                db.collection("job").addDocument(data:["position":position, "company": company, "location": location, "deadline": deadline, "link": link])
            }
        }
    }

}
